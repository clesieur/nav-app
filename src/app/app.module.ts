import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {Page2} from "../pages/page2/page2";
import {Page3} from "../pages/page3/page3";
import {Page41} from "../pages/page41/page41";
import {Page5} from "../pages/page5/page5";
import {Page42} from "../pages/page42/page42";
import {Page43} from "../pages/page43/page43";
import {Page44} from "../pages/page44/page44";
import {TabsPage} from "../pages/tabs/tabs";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Page2,
    Page3,
    Page41,
    Page42,
    Page43,
    Page44,
    Page5,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(
      MyApp,
      {
        tabsHideOnSubPages: true
      }
    )
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Page2,
    Page3,
    Page41,
    Page42,
    Page43,
    Page44,
    Page5,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
