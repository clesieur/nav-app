import {Component, OnDestroy, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Page3} from "../page3/page3";

/**
 * Generated class for the Page2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page2',
  templateUrl: 'page2.html',
})
export class Page2 implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('constructor Page2');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page2');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page2');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page2');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload Page2');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page2');
  }

  ngOnInit(){
    //console.log('ngOnInit Page2');
  }

  Push(){
    this.navCtrl.push(Page3);
  }

}
