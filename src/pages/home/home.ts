import {Component, OnDestroy, OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import {Page2} from "../page2/page2";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController){
    console.log('constructor HomePage');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter HomePage');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave HomePage');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad HomePage');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload HomePage');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy HomePage');
  }

  ngOnInit(){
    //console.log('ngOnInit HomePage');
  }

  public Push(){
    this.navCtrl.push(Page2);
  }

}
