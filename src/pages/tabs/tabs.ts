/**
 * Created by CLesieur on 23/02/2018.
 */

import {Component, OnDestroy, OnInit} from "@angular/core";
import {Page41} from "../page41/page41";
import {Page42} from "../page42/page42";
import {Page43} from "../page43/page43";
import {Page44} from "../page44/page44";
import {NavController} from "ionic-angular";

@Component({
  selector: 'tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnDestroy, OnInit{
  // this tells the tabs component which Pages
  // should be each tab's root Page
  Page41Root: any = Page41;
  Page42Root: any = Page42;
  Page43Root: any = Page43;
  Page44Root: any = Page44;

  constructor(public navCtrl : NavController) {
    console.log('constructor TabsPage');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page44');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page44');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page44');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload Page41');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page44');
  }

  ngOnInit(){
    //console.log('ngOnInit Page44');
  }

}
