import {Component, OnDestroy, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {HomePage} from "../home/home";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the Page3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page3',
  templateUrl: 'page3.html',
})
export class Page3 implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('constructor Page3');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page3');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page3');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page3');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload Page3');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page3');
  }

  ngOnInit(){
    //console.log('ngOnInit Page3');
  }

  PopToRoot(){
    this.navCtrl.popToRoot();
  }

  Pop(){
    this.navCtrl.pop();
  }

  Push(){
    this.navCtrl.push(TabsPage);
  }

  SetRootAndPopToRoot(){
    this.navCtrl.setRoot(HomePage);
    this.navCtrl.popToRoot();
  }

}
