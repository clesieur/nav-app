import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavController, NavParams, NavOptions} from 'ionic-angular';
import {Page2} from "../page2/page2";
import {Page5} from "../page5/page5";

/**
 * Generated class for the Page4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page42',
  templateUrl: 'page42.html',
})
export class Page42 implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('constructor Page42');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page42');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page42');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page42');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload Page41');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page42');
  }

  ngOnInit(){
    //console.log('ngOnInit Page42');
  }

}
