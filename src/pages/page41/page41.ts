import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavController, NavParams, NavOptions, App} from 'ionic-angular';
import {Page2} from "../page2/page2";
import {Page5} from "../page5/page5";
import {TabsPage} from "../tabs/tabs";
import {HomePage} from "../home/home";

/**
 * Generated class for the Page4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page41',
  templateUrl: 'page41.html',
})
export class Page41 implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App) {
    console.log('constructor Page41');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page41');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page41');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page41');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload Page41');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page41');
  }

  ngOnInit(){
    //console.log('ngOnInit Page41');
  }

  SetRootAndPopToRoot(){
    this.app.getRootNav().setRoot(HomePage);
  }

}
