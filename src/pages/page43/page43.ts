import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavController, NavParams, NavOptions} from 'ionic-angular';
import {Page2} from "../page2/page2";
import {Page5} from "../page5/page5";

/**
 * Generated class for the Page4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page43',
  templateUrl: 'page43.html',
})
export class Page43 implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('constructor Page43');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page43');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page43');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page43');
  }


  ionViewWillUnload(){
    console.log('ionViewWillUnload Page41');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page43');
  }

  ngOnInit(){
    //console.log('ngOnInit Page43');
  }

}
