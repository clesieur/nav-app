import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavController, NavParams, NavOptions} from 'ionic-angular';
import {Page2} from "../page2/page2";
import {Page5} from "../page5/page5";

/**
 * Generated class for the Page4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page44',
  templateUrl: 'page44.html',
})
export class Page44 implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('constructor Page4');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page44');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page44');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page44');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload Page41');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page44');
  }

  ngOnInit(){
    //console.log('ngOnInit Page44');
  }

}
