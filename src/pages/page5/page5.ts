import {Component, OnDestroy, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Page3} from "../page3/page3";

/**
 * Generated class for the Page5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page5',
  templateUrl: 'page5.html',
})
export class Page5 implements OnDestroy, OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('constructor Page5');
  }

  ionViewWillEnter() {
    //console.log('ionViewWillEnter Page5');
  }

  ionViewWillLeave() {
    //console.log('ionViewWillLeave Page5');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Page5');
  }

  ionViewWillUnload(){
    console.log('ionViewWillUnload Page5');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy Page5');
  }

  ngOnInit(){
    //console.log('ngOnInit Page5');
  }

  Push(){
    this.navCtrl.push(Page3);
  }

}
